<name> ::= ([A-Z] | [a-z])+ ([A-Z] | [a-z] | [0-9])*	        //names of objects 
<index> ::= ([A-Z] | [a-z])+ ([A-Z] | [a-z] | [0-9])*	        //index text in action
<space> ::= (" " | "\t")+
<space_opt> ::= (" " | "\t")*
<coma> ::= <space_opt> "," <space_opt>
<semicolon> ::= <space_opt> ";" <space_opt>
<colon> ::= <space_opt> ":" <space_opt>
<comment> ::= <space> "/*" ([A-Z] | [a-z] | [0-9] | " " | "\t" | "\n")* "*/" <space_opt>
<endline> ::=  (<comment>)* <space_opt> ("\n")+


<list_of_obj> ::= "[" <space_opt> <list_o> <space_opt> "]"
<list_o> ::= <list_o> <coma> <name>
        | <name>
<list_of_index> ::= "[" <space_opt> <list_i> <space_opt> "]" 
<list_i> ::= <list_i> <space_opt> <coma> <space_opt> <index> <space_opt>
        | <index> 

<start> ::= <body> "EOF"	//startup symbol

<body> ::= <body> <space_opt> <operation>	                //body of code with valid whitespaces
        | <space_opt> <operation>

<operation> ::= <object>	                                //all main operations
	| <start_point>
        | <many_connection>
        | <loop>
        | <end_point>
        | <comment>
        | "\n"

<object> ::=  "obj" <space> <name> <endline>		        //declaration of object
<start_point> ::= "start" <space> <name> <endline>		//start point of object
<end_point> ::= "end" <space> <name> <endline> 		        //end point of object
<many_connection> ::= <connection> <many_connection> 
                |  <connection> 

<connection> ::= <space_opt> <direction> <space_opt> ":" <space_opt> <index> <endline>		//body of connection e.g. A->B:idx
<direction> ::= <name> <space_opt> <out_arrow> <space_opt> <name> 		                //e.g. A->B
        | <name> <space_opt> <in_arrow> <space_opt>
<out_arrow> ::= "->" 
        | "-->" 
        | "..>" 
<in_arrow> ::= "<-" 
        | "<--" 
        | "<.." 
        
<loop> ::= "pattern" <space> <pattern_body> 	//body of pattern, our loop operations
<pattern_body> ::= <list_of_obj> <semicolon> <list_of_index> <semicolon> <list_of_index> <colon> <endline> <loop_body_with_cases>
                                                                                        //1st possible of pattern e.g. [A,B]; [aaa,bbb]; [a, b]
                | <list_of_obj> <semicolon> <list_of_index> <colon> <endline> <loop_body_without_cases>		
                                                                                        //2nd possible of pattern e.g. [A,B]; [aaa,bbb]

<loop_body_with_cases> ::=  <cases> <space_opt> "end pattern" <endline>		                //body of 1st pattern
<loop_body_without_cases> ::= <no_cases> <space_opt> "end pattern" <endline>	                //body of 2nd pattern

<cases> ::= <space_opt> "case" <space> <index> <colon> <pattern_many_connection> <cases>	//cases appear only in 2nd pattern
        | <space_opt> "case" <space> <index> <colon> <pattern_many_connection> 
        | <no_cases>		                                                                //no cases are allowed in 2nd pattern
<no_cases> ::= <pattern_many_connection> <no_cases> 
        | <pattern_many_connection>

<pattern_many_connection> ::= <pattern_connection> <pattern_many_connection>	//special connection for patterns, it declares pattern's keywords too
			| <connection> <pattern_many_connection>
			| <connection>
                	| <pattern_connection>
<pattern_connection> ::= <space_opt> <pattern_direction> <space_opt> <colon> <space_opt> <pattern_index> <endline> 
<pattern_direction> ::= <pattern_name> <space_opt> <out_arrow> <space_opt> <pattern_name>
		| <pattern_name> <space_opt> <in_arrow> <space_opt>
<pattern_name> ::= "pre"
		| "nxt"
                | <name>
<pattern_index> ::= "stp"
		| <index>
